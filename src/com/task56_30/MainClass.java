package com.task56_30;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RandomNumber myRandomDoubleNumber = new RandomNumber();
		System.out.println("Random value in double from 1 to 100: " + myRandomDoubleNumber.getRandomDoubleNumber(1,100));
		RandomNumber myRandomIntNumber = new RandomNumber();
		System.out.println("Random value in int from 1 to 10: " + myRandomIntNumber.getRandomIntNumber(1, 10));
	}

}
