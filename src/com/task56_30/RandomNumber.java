package com.task56_30;

import java.util.Random;

public class RandomNumber {
public RandomNumber() {
	
}
public double getRandomDoubleNumber(int rangeMin, int rangeMax) {
	Random r = new Random();
	double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
	return randomValue;
}

public int getRandomIntNumber(int rangeMin, int rangeMax) {
	Random r = new Random();
	 int randomIntNum = r.nextInt((rangeMax - rangeMin) + 1) + rangeMin;
	return randomIntNum;
}
}
